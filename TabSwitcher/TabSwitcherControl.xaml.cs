﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TabSwitcher
{
    /// <summary>
    /// Логика взаимодействия для TabSwitcherControl.xaml
    /// </summary>
    public partial class TabSwitcherControl : UserControl
    {
        public TabSwitcherControl()
        {
            InitializeComponent();
            ComboBox1.DropDownOpened += OnClick;
            Button1.Click += OnClickButtons;
            Button2.Click += OnClickButtons;
            Button3.Click += OnClickButtons;
        }

        private void OnClick(object sender, EventArgs e)
        {
            if (Click != null) Click(sender, e);
        }

        private void OnClickButtons(object sender, EventArgs e)
        {
            if (ClickButtons != null) ClickButtons(sender, e);
            OnClick(sender, e);
        }

        public event EventHandler Click;

        public event EventHandler ClickButtons;

        public string Text
        {
            get { return ComboBox1.Text; }
            set
            {
                ComboBox1.Items.Add(value);
                ComboBox1.Text = value;
            }

        }

        public ItemCollection Items 
        { 
            get { return ComboBox1.Items; }
        }

        public object SelectedItem 
        {
            get { return ComboBox1.SelectedItem; }
        }
    }
}
