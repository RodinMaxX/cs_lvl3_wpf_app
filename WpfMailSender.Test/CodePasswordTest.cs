﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using MailSender;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WpfMailSender.Test
{
    [TestClass]
    public class CodePasswordTest
    {
        [TestInitialize]
        public void TestInitialize ()
        {
        }

        [TestMethod]
        public void getCodPassword_abc_bcd()
        {
            // arrange
            string strIn = "abc";
            string strExpected = "bcd";
            // act
            Debug.WriteLine("p_sPassword=" + strIn);
            string strActual = PasswordClass.getCodPassword(strIn);
            //assert
            Assert.AreEqual(strExpected, strActual, "\nIncorrect decoding password!");
        }
      
        [TestMethod]
        public void getCodPassword_aaa_bbb()
        {
            // arrange
            string strIn = "aaa";
            string strExpected = "b";
            // act
            Debug.WriteLine("p_sPassword=" + strIn);
            string strResult = PasswordClass.getCodPassword(strIn);
            //assert
            StringAssert.StartsWith(strResult,strExpected, "\nWrong decrypt same method!");
        }

        [TestCleanup]
        public void TestCleanup ()
        {
            
        }
    }
}
