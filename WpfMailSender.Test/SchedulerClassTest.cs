﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using MailSender;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WpfMailSender.Test
{
    [TestClass]
    public class SchedulerClassTest
    {
        private SchedulerClass sc;
        private TimeSpan tsZero;

        [TestInitialize]
        public void TestInitialize()
        {
            Debug.WriteLine("Test Initialize");
            sc = new SchedulerClass();
            tsZero = new TimeSpan(); // возвращаем в случае ошибочно введенного времени
            sc.DatesEmailTexts = new Dictionary<DateTime, string>()
            {
                {new DateTime(2016, 12, 24, 22, 0, 0), "text1"},
                {new DateTime(2016, 12, 24, 22, 30, 0), "text2"},
                {new DateTime(2016, 12, 24, 23, 0, 0), "text3"}
            };
        }

        [TestMethod()]
        public void TimeTick_Dictionary_Correct()
        {
            DateTime dt1 = new DateTime(2016, 12, 24, 22, 0, 0);
            DateTime dt2 = new DateTime(2016, 12, 24, 22, 30, 0);
            DateTime dt3 = new DateTime(2016, 12, 24, 23, 0, 0);

            if (sc.DatesEmailTexts.Keys.First<DateTime>().ToShortTimeString() == dt1.ToShortTimeString())
            {
                Debug.WriteLine("Body " + sc.DatesEmailTexts[sc.DatesEmailTexts.Keys.First<DateTime>()]);
                sc.DatesEmailTexts.Remove(sc.DatesEmailTexts.Keys.First<DateTime>());

            }

            if (sc.DatesEmailTexts.Keys.First<DateTime>().ToShortTimeString() == dt2.ToShortTimeString())
            {
                Debug.WriteLine("Body " + sc.DatesEmailTexts[sc.DatesEmailTexts.Keys.First<DateTime>()]);
                sc.DatesEmailTexts.Remove(sc.DatesEmailTexts.Keys.First<DateTime>());
            }

            if (sc.DatesEmailTexts.Keys.First<DateTime>().ToShortTimeString() == dt3.ToShortTimeString())
            {
                Debug.WriteLine("Body " + sc.DatesEmailTexts[sc.DatesEmailTexts.Keys.First<DateTime>()]);
                sc.DatesEmailTexts.Remove(sc.DatesEmailTexts.Keys.First<DateTime>());
            }

            Assert.AreEqual(0, sc.DatesEmailTexts.Count);
        }

        [TestMethod()]
        public void TimeTick_Dictionary_Add()
        {
            DateTime dt4 = new DateTime(2016, 12, 24, 23, 30, 0);
            var tsActualPart = new Dictionary<DateTime, string> {{dt4, "test4"}};
            sc.DatesEmailTexts.Add(tsActualPart.Single().Key, tsActualPart.Single().Value);
            CollectionAssert.Contains(sc.DatesEmailTexts, tsActualPart.Single(), "No actual element in collection");
        }
    }
}
