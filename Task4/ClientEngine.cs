﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Task4
{
   public class ClientEngine
    {
        private SqlLocalDbContext context;

        public ClientEngine()
        {
             context = new SqlLocalDbContext();
        }

        public void ImportCsv(string file)
        {
            using (var input = new StreamReader(file, Encoding.Default))
            {
                while (!input.EndOfStream)
                {
                 var client = new Client(input.ReadLine());
                    if (!String.IsNullOrEmpty(client.FullName))
                    {
                        context.Clients.Add(client);
                    }
                }
            }
            Save();
            context.Clients.Load();
        }

       public object GetBinding ()
       {
           context.Clients.Load();
           return context.Clients.Local.ToBindingList();
       }

       public void Save()
       {
           context.SaveChanges(); 
       }

       public void DeleteRow(object id)
       {
           context.Clients.Remove(context.Clients.Find(id));
       }
    }
}
