﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Task4
{
    public partial class Form1 : Form
    {
        private ClientEngine clients;

        public Form1()
        {
            InitializeComponent();
            clients = new ClientEngine();
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = clients.GetBinding();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            openFileDialog1.ShowDialog();
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            clients.Save();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow != null)
            {
                clients.DeleteRow(dataGridView1.CurrentRow.Cells["IdColumn"].Value);
            }
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            var file = openFileDialog1.FileName;
            clients.ImportCsv(file);
        }
    }
}
