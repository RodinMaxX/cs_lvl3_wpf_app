﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Windows.Forms.VisualStyles;

namespace Task4
{
    public class SqlLocalDbContext : DbContext
    {
        public SqlLocalDbContext() 
            : base("SqlLocalDatabaseConnection")
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<SqlLocalDbContext>());
        }
        public virtual DbSet<Client> Clients { get; set; }
    }
}
