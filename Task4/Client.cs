﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;

namespace Task4
{
    public class Client
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        public string FullName { get; set; }
        
        public string  Email { get; set; }
        
        public string Phone { get; set; }

        public override string ToString()
        {
            return String.Join(";",FullName, Email, Phone);
        }

        public Client(string csvLine)
        {
            string[] a = csvLine.Split(';');
           // Id = id;
            FullName = a[0];
            Email = a[1];
            Phone = a[2];
        }

        public Client()
        {
            
        }



    }
}
