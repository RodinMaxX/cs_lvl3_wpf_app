﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EASendMail;


namespace MailSender
{
    public class EmailSendServiceClass
    {
        public static string Send(Account sender, ICollection<Client> clientList, MailTemplate mail)
        {
            var mm = new SmtpMail("TryIt");
            mm.From = sender.From;
            mm.Subject = mail.Subject;
            mm.TextBody = mail.Text;
            if (!String.IsNullOrWhiteSpace(mail.Filepath))
            {
                mm.AddAttachment(mail.Filepath);
            }
            foreach (var client in clientList)
            {
                mm.To.Add(new MailAddress(client.To));
            }
            SmtpServer sc = new SmtpServer(sender.ServerUri.Host);
            sc.Port = sender.ServerUri.Port;
            sc.ConnectType = SmtpConnectType.ConnectSSLAuto;
            sc.User = sender.From;
            sc.Password = sender.Password;
            try
            {
                var smtp = new SmtpClient();
                smtp.SendMail(sc, mm);
                return null;
            }
            catch (Exception ex)
            {
                var error = "Не удалось отправить письмо:";
                do
                {
                    error += Environment.NewLine + ex.Message;
                    ex = ex.InnerException;
                } while (ex != null);
                return error;
            }
        }

        public void SendMails(IQueryable<Emails2> emails)
        {
            foreach (var mail in emails)
            {
                Send(mail.Account, mail.Clients, mail.Content);
            }
        }
    }
}
