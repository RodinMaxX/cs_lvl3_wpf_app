using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MailSender
{
    public class Emails2
    {
        [Key]
        public int EmailId { get; set; }
        
        public MailTemplate Content { get; set; }
        public Account Account { get; set; }
        public ICollection<Client> Clients { get; set; }
    }
}