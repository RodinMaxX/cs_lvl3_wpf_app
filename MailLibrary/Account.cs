﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MailSender
{
    public class Account
    {
        public string From;
        public string Password;
        public Uri ServerUri;

        public override string ToString()
        {
            return From;
        }
    }

}
