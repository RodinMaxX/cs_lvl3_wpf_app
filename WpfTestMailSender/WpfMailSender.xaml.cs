﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;


namespace MailSender
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class WpfMailSender : Window
    {
        public WpfMailSender()
        {
            InitializeComponent();
            ChosenSender.Items.Add(new Account
                                       {
                                           From = "r_maks@autorambler.ru",
                                           Password = "123456",
                                           ServerUri = new Uri("smtp://smtp.rambler.ru:465"),
                                       });
        }

        private void SendEmailClick(object sender, RoutedEventArgs e)
        {
            var account = ChosenSender.SelectedItem as Account; 
            TextRange textRange = new TextRange(MailContent.Document.ContentStart, MailContent.Document.ContentEnd);
          var listStrMails = new List<Client>();
                foreach (
                    var clientTo in ClientList.Text.Split("\n\r".ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                {
                    listStrMails.Add(new Client {To = clientTo});
                }  
            if (account == null)
            {
                Tabs.SelectedItem = SenderTab;
                MessageBox.Show("Отправитель не задан");
            }
            else if (string.IsNullOrWhiteSpace(textRange.Text))
{
            
             Tabs.SelectedItem = MailEditor; 
                MessageBox.Show("Письмо не заполнено");
            }
            else if (listStrMails.Count ==0)
            {
                Tabs.SelectedItem = SenderTab;
                MessageBox.Show("Необходимо задать адресата");
            }
            else
            {
            var mail = new MailTemplate
                               {
                                   Subject = Subject.Text,
                                   Text = textRange.Text,
                               };
                var error = EmailSendServiceClass.Send(account, listStrMails, mail);
                if (error == null)
                {
                    SendEndWindow sendEndWindow = new SendEndWindow();
                    sendEndWindow.Show();
                }
                else
                {
                    ErrorWindow errorWindow = new ErrorWindow();
                    errorWindow.Description.Content = error;
                    errorWindow.Show();
                }
            }
        }

        private void ExitClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void SchedulerClick(object sender, RoutedEventArgs e)
        {
            Tabs.SelectedItem = SchedulerTab;
        }

        private void SmtpServerChanged(object sender, EventArgs e)
        {
            MessageBox.Show("Данная функция устарела: smtp-сервер определяется именем отправителя и задаётся в его настройках.");
        }

        private void EditSmtpServer(object sender, EventArgs routedEventArgs)
        {
            SmtpServerChanged(sender, null);
        }

        private void ChooseClientClick(object sender, EventArgs e)
        {
            MessageBox.Show("Задайте одного или нескольких получателей письма в расположенном ниже поле ввода - каждого в отдельной строке.");
        }

        private void FuncBlockedClick(object sender, EventArgs e)
        {
            MessageBox.Show("Данная функция будет доступна после оплаты подписки");
        }
    }
}