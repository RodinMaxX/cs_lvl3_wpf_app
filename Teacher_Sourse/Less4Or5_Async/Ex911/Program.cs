﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ex911
{

    class Program
    {
        //Когда метод GetAvailableThreads возвращает значение, 
        //переменная, указанная параметром workerThreads, 
        //содержит число дополнительных рабочих потоков, 
        //которые могут быть запущены, а переменная, указанная параметром 
        //completionPortThreads, содержит число дополнительных 
        //потоков асинхронного ввода/вывода, которые могут быть запущены.

        // Если доступные потоки отсутствуют, дополнительные 
        // запросы к пулу потоков будут оставаться в очереди до тех пор, 
        // пока в пуле потоков не появятся доступные потоки.
        static void ShowThreadInfo()
        {
            int availableWorkThreads, availableIOThreads, maxWorkThreads, maxIOThreads;
            ThreadPool.GetAvailableThreads(out availableWorkThreads, out availableIOThreads);
            ThreadPool.GetMaxThreads(out maxWorkThreads, out maxIOThreads);
            Console.WriteLine($"-Доступно рабочих потоков в пуле:{availableWorkThreads} из {maxWorkThreads}" );
            Console.WriteLine($"-Доступно потоков ввода-вывода в пуле:{availableIOThreads} из {maxIOThreads}\n");
        }
        static void Main()
        {
            //ThreadPool.SetMinThreads(2, 2);
            //ThreadPool.SetMaxThreads(5, 5);
            Console.WriteLine("start");
            ShowThreadInfo();
   
            Console.WriteLine("start Task1 ThreadPool");
            ThreadPool.QueueUserWorkItem(Task1);
            ShowThreadInfo();

            Console.WriteLine("start Task2 ThreadPool");
            Thread.Sleep(1000);
            ThreadPool.QueueUserWorkItem(Task2);
            ShowThreadInfo();

            Console.WriteLine("main");
            Thread.Sleep(1000);
            Console.WriteLine("end\n");

            // Delay.
            Console.WriteLine("Task1 и Task2 закончили работу");
            ShowThreadInfo();
            Console.ReadKey();
            ShowThreadInfo();

            

        }

        static void Task1(Object state)
        {
            Thread.CurrentThread.Name = "1";
            Console.WriteLine($"Поток {Thread.CurrentThread.Name}:{Thread.CurrentThread.ManagedThreadId}");
            Console.WriteLine();
            Thread.Sleep(200);
        }

        static void Task2(Object state)
        {
            Thread.CurrentThread.Name = "2";
            Console.WriteLine($"Поток {Thread.CurrentThread.Name}:{Thread.CurrentThread.ManagedThreadId}");
            Thread.Sleep(200);
        }


    }

}
