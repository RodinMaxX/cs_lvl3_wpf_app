﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ex8
{
    class Program
    {
        // Объект для блокировки.
        static private readonly object block = new object();

        // Счетчик потоков.
        static private int counter;
        static private readonly Random random = new Random();

        // Выполняется в отдельном потоке.
        private static void M1()
        {
            try
            {
                Monitor.Enter(block); // Начало блокировки.
                ++counter;
            }
            finally
            {
                Monitor.Exit(block);  // Конец блокировки.
            }

            
            Thread.Sleep(random.Next(1000, 10000));

            try
            {
                Monitor.Enter(block); // Начало блокировки.
                --counter;
            }
            finally
            {
                Monitor.Exit(block);  // Конец блокировки.
            }
        }

        // Мониторинг количества запущеных потоков.
        private static void Report()
        {
            while (true)
            {
                int count;
                try
                {
                    Monitor.Enter(block);// Начало блокировки.
                    count = counter;
                }
                finally
                {
                    Monitor.Exit(block);
                }
                Console.WriteLine($"{count} поток(ов) активно {counter}");
                Thread.Sleep(100);
            }
        }

        static void Main()
        {
            var reporter = new Thread(Report) { IsBackground = true };
            reporter.Start();

            var threads = new Thread[150];

            for (uint i = 0; i < threads.Length; ++i)
            {
                threads[i] = new Thread(M1);
                threads[i].Start();
            }
            Thread.Sleep(15000);
        }
    }
}