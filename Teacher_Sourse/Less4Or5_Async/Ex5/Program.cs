﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


// Есть два варианта работы потоков Foreground и Background
// Foreground - Будет работать после завершения работы первичного потока.
// Background - Завершает работу вместе с первичным потоком.


namespace Ex5
{
    class Program
    {
        private static void M1()
        {
            for (int i = 0; i < 500; i++)
            {
                Thread.Sleep(10);
                Console.Write(".");
            }
            Console.WriteLine($"\nM1HashCode {Thread.CurrentContext.GetHashCode()}");
        }

        static void Main()
        {
            var thread = new Thread(M1);

            // IsBackground - устанавливает поток как фоновый. 
            // Не ждем завершения вторичного потока в данном случае.

            thread.IsBackground = true; 
            thread.Start();

            Thread.Sleep(500);

            Console.WriteLine($"\nMainHashCode {Thread.CurrentContext.GetHashCode()}");
        }
    }
}
