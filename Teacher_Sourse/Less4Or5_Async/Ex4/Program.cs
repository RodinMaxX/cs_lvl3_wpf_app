﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ex4
{
    class Program
    {
        private static void Function()
        {
            while (true)
            {
                try
                {
                    Thread.Sleep(10);
                    Console.Write("+");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"\n {ex.GetType()}");

                    for (int i = 0; i < 10; i++)
                    {
                        Thread.Sleep(20);
                        Console.Write(".");
                    }
                    // Если не вызывать Thread.ResetAbort() - исключение повторно 
                    // сгенерируется после выхода из catch{}
                    // Предотвращение повторной генерации ThreadAbortException!
                    Thread.ResetAbort();
                }

            }
        }

        static void Main()
        {
            Thread thread = new Thread(new ThreadStart(Function));
            thread.Start();

            Thread.Sleep(2000);
            thread.Abort();

            Console.WriteLine("\n__________________________");

        }
    }
}
