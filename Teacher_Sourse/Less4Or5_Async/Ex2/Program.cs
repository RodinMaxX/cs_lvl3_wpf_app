﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ex2
{
    class Program
    {
        private static void WriteString(string Str, int Count)
        {
            for (int i = 0; i < Count; i++)
            {
                Thread.Sleep(20);
                Console.Write(Str);
            }
        }

        // Метод выполняющийся в третичном потоке.(Запуск из вторичного потока.)
        public static void M3()
        {
            Console.WriteLine($"\nM3HashCode start {Thread.CurrentThread.GetHashCode()}");

            WriteString("3 ", 200);
            Console.WriteLine($"\nM3HashCode start {Thread.CurrentThread.GetHashCode()}");

        }

        // Метод выполняющийся во вторичном потоке.(Запуск из первичного потока.)
        public static void M2()
        {
            Console.WriteLine($"\nM2HashCode start {Thread.CurrentThread.GetHashCode()}");

            WriteString("2 ", 200);

            // Создание потока с M3.
            var thread = new Thread(M3);
            thread.Start();
            //thread.Join();

            WriteString("2 ", 200);

            Console.WriteLine($"\nM2HashCode start {Thread.CurrentThread.GetHashCode()}");
        }


        // Метод выполняющийся в первичном потоке.
        static void Main()
        {
            Console.WriteLine($"MainHashCode start {Thread.CurrentThread.GetHashCode()}");

            WriteString("1 ", 200);

            // Создание потока с M2
            var thread = new Thread(M2);
            thread.Start();

            //thread.Join();

            WriteString("1 ", 200);

            Console.WriteLine($"\nMainHashCode end {Thread.CurrentThread.GetHashCode()}");


            Console.ReadKey();
        }
    }
}
