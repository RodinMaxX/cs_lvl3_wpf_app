﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ex9
{
    class Program
    {
        static private readonly object block = new object();
        static private int counter;
        static private readonly Random random = new Random();

        private static void Function()
        {
            lock (block)
            {
                ++counter;
            }

            Thread.Sleep(random.Next(1, 100));

            lock (block)
            {
                --counter;
            }
        }

        private static void Report()
        {
            while (true)
            {
                int count;

                lock (block)
                {
                    count = counter;
                }

                Console.WriteLine($"{count} поток(ов) активно {counter}");
                Thread.Sleep(100);
            }
        }

        static void Main()
        {
            var reporter = new Thread(Report) { IsBackground = true };
            reporter.Start();

            var threads = new Thread[150];

            for (uint i = 0; i < threads.Length; ++i)
            {
                threads[i] = new Thread(Function);
                threads[i].Start();
            }

            Thread.Sleep(12000);
        }
    }
}
