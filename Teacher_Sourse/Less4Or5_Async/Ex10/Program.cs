﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ex10
{
    class Program
    {
        static private int counter = 0;

        // Нельзя использовать объекты блокировки структурного типа.
        // block - не может быть структурным.
        static private int block = 0;

        static private void M1()
        {
            Thread.Sleep(5000);
            for (int i = 0; i < 50; ++i)
            {
                // Устанавливается блокировка постоянно в новый object (boxing).
                Monitor.Enter((object)block);
                try
                {
                    Console.WriteLine(++counter);
                }
                finally
                {
                    // Попытка снять блокировку с незаблокированного объекта (boxing создает новый объект).
                    Monitor.Exit((object)block);
                }
            }
        }

        static void Main()
        {


            Console.ReadLine();

            Thread[] threads = { new Thread(M1), new Thread(M1), new Thread(M1) };

            foreach (Thread t in threads)
            {
                t.Start();
            }
            Console.ReadLine();

            //// Задержка.
            //Console.ReadKey();
        }
    }
}
