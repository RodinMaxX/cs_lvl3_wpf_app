﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Threading;

namespace ConsoleApp1
{
    class A
    {
        private string s = "new s";
        ////public void M(A b, string val)
        ////{
        ////    b.s = val;
        ////}
        public string S => s;
    }


    class Program
    {
        static void M<T>(T a)
        {
            typeof(T).GetField("s", BindingFlags.Instance | BindingFlags.NonPublic)
                     .SetValue(a, "qqqq");
        }

        static void Method()
        {
            for (int i = 0; i < 200; i++)
            {
                Console.Write("+");
                Thread.Sleep(10);

            }
        }
        static void Main(string[] args)
        {
            


            Thread t = new Thread(Method);
            t.Start();

            for (int i = 0; i < 200; i++)
            {
                Console.Write(".");
                Thread.Sleep(10);
            }


           

            #region A

            //CLR
            //A a = new A();
            //Console.WriteLine(a.S);

            //M<A>(a);

            ////a.M(a, "caca");

            //Console.WriteLine(a.S);

            #endregion

        }
    }
}
