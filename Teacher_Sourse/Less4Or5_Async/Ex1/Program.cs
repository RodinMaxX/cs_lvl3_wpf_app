﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ex1
{

    class Program
    {
        static void M1()
        {
            Console.WriteLine($"M1HashCode start {Thread.CurrentThread.GetHashCode()}");

            for (int i = 0; i < 100; i++)
            {
                Thread.Sleep(20);
                Console.Write(".");
            }
            Console.WriteLine($"\nM1HashCode end {Thread.CurrentThread.GetHashCode()}");
        }

        static void Main(string[] args)
        {
            Console.ReadLine();
            Console.WriteLine($"\nMainHashCode start {Thread.CurrentThread.GetHashCode()}");
            // Создание нового потока.
            var thread = new Thread(new ThreadStart(M1));
            //var thread = new Thread(M1);
            thread.Start();
 

            #region Join

            //Ожидание первичным потоком, завершения работы вторичного потока
            thread.Join();  

            #endregion

            for (int i = 0; i < 100; i++)
            {
                Thread.Sleep(20);
                Console.Write("-");
            }

            Console.WriteLine($"\nMainHashCode end {Thread.CurrentThread.GetHashCode()}");


            // Задержка.
            Console.ReadKey();
        }
    }
}
