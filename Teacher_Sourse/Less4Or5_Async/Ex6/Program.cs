﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ex6
{
    // Приоритеты потоков. 

    class ThreadWork
    {
        public Thread RuningThread;
        static bool isStop;
        readonly ConsoleColor color;

        public int Count { get; set; }

        public ThreadWork(string name, ConsoleColor color)
        {
            RuningThread = new Thread(Run) { Name = name };
            this.color = color;
            Console.ForegroundColor = this.color;
            Console.WriteLine("Поток " + RuningThread.Name + " начат.");
        }

        void Run()
        {
            do
            {
                Count++;
            }

            while (isStop == false && Count < 50_000_000);

            isStop = true;
            Console.WriteLine("\nПоток " + RuningThread.Name + " завершен.");
        }

        public void BeginInvoke()
        {
            RuningThread.Start();
        }

        public void EndInvoke()
        {
            RuningThread.Join();
        }

        public ThreadPriority Priority
        {
            set { RuningThread.Priority = value; }
        }
    }

    class Program
    {
        static void Main()
        {
            Console.SetWindowSize(60, 20);

            var work1 = new ThreadWork("Thread Priority - Highest", ConsoleColor.Red);
            var work2 = new ThreadWork("Thread Priority - Lowest", ConsoleColor.Yellow);

            // Установить приоритеты для потоков. 
            work1.Priority = ThreadPriority.Highest;
            work2.Priority = ThreadPriority.Lowest;

            work1.BeginInvoke();
            work2.BeginInvoke();
          


            work1.EndInvoke();
            work2.EndInvoke();

            Console.WriteLine();
            Console.WriteLine($"Поток {work1.RuningThread.Name} досчитал до {work1.Count.ToString("## ### ###")}");
            Console.WriteLine($"Поток {work2.RuningThread.Name} досчитал до {work2.Count.ToString("## ### ###")}");


            // Delay.
            Console.ReadKey();
        }
    }
}
