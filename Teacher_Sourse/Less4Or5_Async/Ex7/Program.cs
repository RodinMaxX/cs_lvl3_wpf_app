﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ex7
{
    class Program
    {
        // Счетчик запущеных потоков.
        static private long counter;
        static private readonly Random random = new Random();

        private static void M1()
        {
            // Поток увеличивает счетчик.
            //Interlocked.Increment(ref counter);

            counter++;

            Thread.Sleep(random.Next(80,85));
            
            // Поток уменьшает счетчик. 
            //Interlocked.Decrement(ref counter);
            counter--;
        }

        // Проверка количества запущеных потоков. 
        private static void Report()
        {
            while (true)
            {
                long number = Interlocked.Read(ref counter);

                Console.WriteLine($"{number} поток(ов) активно. counter = {counter} ");
                Thread.Sleep(100);
            }
        }

        static void Main()
        {
            var reporter = new Thread(Report) { IsBackground = true };
            reporter.Start();

            var threads = new Thread[150];

            for (uint i = 0; i < threads.Length; ++i)
            {
                threads[i] = new Thread(M1);
                threads[i].Start();
            }

            Thread.Sleep(2500);
        }
    }
}
