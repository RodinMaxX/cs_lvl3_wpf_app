using System.Data.Entity;
using Task3;

namespace Lesson7HomeworkTask3
{
    public class CinemaEngine
    {
        private SqlLocalDbContext context;

        public CinemaEngine()
        {
            context = new SqlLocalDbContext();
        }

        public object GetBinding()
        {
            context.Films.Include(x => x.Buys).Load();
            return context.Films.Local.ToBindingList();
        }

        public void Save()
        {
            context.SaveChanges(); 
        }

        public void DeleteRow(object id)
        {
            context.Films.Remove(context.Films.Find(id));
        }

        public void AddBuy(Buy buy)
        {
           context.Buys.Add(buy);
        }
    }
}