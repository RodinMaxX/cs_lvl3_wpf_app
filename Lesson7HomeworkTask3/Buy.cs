using System;

namespace Task3
{
    public class Buy
    {
        public int Id {get; set;}
        public int FilmId { get; set; }
        public int Count { get; set; }
        public DateTime Time { get; set; }

        public Buy()
        {
        }

        public Buy(Film film, int count, DateTime time)
        {
            Count = count;
            Time = time;
            FilmId = film.Id;
        }
    }
}