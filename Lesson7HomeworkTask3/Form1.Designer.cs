﻿namespace Lesson7HomeworkTask3
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.Tabs = new System.Windows.Forms.TabControl();
            this.TicketsTab = new System.Windows.Forms.TabPage();
            this.BuyButton = new System.Windows.Forms.Button();
            this.TicketsCount = new System.Windows.Forms.NumericUpDown();
            this.TicketsCountLabel = new System.Windows.Forms.Label();
            this.FilmComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.FilmsTab = new System.Windows.Forms.TabPage();
            this.DeleteFilm = new System.Windows.Forms.Button();
            this.SaveAll = new System.Windows.Forms.Button();
            this.FilmsTable = new System.Windows.Forms.DataGridView();
            this.IdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FilmNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuysColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuysCountColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BeginTimeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tabs.SuspendLayout();
            this.TicketsTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TicketsCount)).BeginInit();
            this.FilmsTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FilmsTable)).BeginInit();
            this.SuspendLayout();
            // 
            // Tabs
            // 
            this.Tabs.Controls.Add(this.TicketsTab);
            this.Tabs.Controls.Add(this.FilmsTab);
            this.Tabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tabs.Location = new System.Drawing.Point(0, 0);
            this.Tabs.Name = "Tabs";
            this.Tabs.SelectedIndex = 0;
            this.Tabs.Size = new System.Drawing.Size(585, 484);
            this.Tabs.TabIndex = 0;
            // 
            // TicketsTab
            // 
            this.TicketsTab.Controls.Add(this.BuyButton);
            this.TicketsTab.Controls.Add(this.TicketsCount);
            this.TicketsTab.Controls.Add(this.TicketsCountLabel);
            this.TicketsTab.Controls.Add(this.FilmComboBox);
            this.TicketsTab.Controls.Add(this.label1);
            this.TicketsTab.Location = new System.Drawing.Point(4, 22);
            this.TicketsTab.Name = "TicketsTab";
            this.TicketsTab.Padding = new System.Windows.Forms.Padding(3);
            this.TicketsTab.Size = new System.Drawing.Size(577, 458);
            this.TicketsTab.TabIndex = 0;
            this.TicketsTab.Text = "Билеты";
            this.TicketsTab.UseVisualStyleBackColor = true;
            // 
            // BuyButton
            // 
            this.BuyButton.Location = new System.Drawing.Point(14, 161);
            this.BuyButton.Name = "BuyButton";
            this.BuyButton.Size = new System.Drawing.Size(75, 23);
            this.BuyButton.TabIndex = 4;
            this.BuyButton.Text = "Купить!";
            this.BuyButton.UseVisualStyleBackColor = true;
            this.BuyButton.Click += new System.EventHandler(this.BuyButton_Click);
            // 
            // TicketsCount
            // 
            this.TicketsCount.Location = new System.Drawing.Point(14, 103);
            this.TicketsCount.Name = "TicketsCount";
            this.TicketsCount.Size = new System.Drawing.Size(56, 20);
            this.TicketsCount.TabIndex = 3;
            // 
            // TicketsCountLabel
            // 
            this.TicketsCountLabel.AutoSize = true;
            this.TicketsCountLabel.Location = new System.Drawing.Point(11, 87);
            this.TicketsCountLabel.Name = "TicketsCountLabel";
            this.TicketsCountLabel.Size = new System.Drawing.Size(49, 13);
            this.TicketsCountLabel.TabIndex = 2;
            this.TicketsCountLabel.Text = "Билетов";
            // 
            // FilmComboBox
            // 
            this.FilmComboBox.FormattingEnabled = true;
            this.FilmComboBox.Location = new System.Drawing.Point(11, 51);
            this.FilmComboBox.Name = "FilmComboBox";
            this.FilmComboBox.Size = new System.Drawing.Size(121, 21);
            this.FilmComboBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Киносеанс";
            // 
            // FilmsTab
            // 
            this.FilmsTab.Controls.Add(this.DeleteFilm);
            this.FilmsTab.Controls.Add(this.SaveAll);
            this.FilmsTab.Controls.Add(this.FilmsTable);
            this.FilmsTab.Location = new System.Drawing.Point(4, 22);
            this.FilmsTab.Name = "FilmsTab";
            this.FilmsTab.Padding = new System.Windows.Forms.Padding(3);
            this.FilmsTab.Size = new System.Drawing.Size(577, 458);
            this.FilmsTab.TabIndex = 1;
            this.FilmsTab.Text = "Киносеансы";
            this.FilmsTab.UseVisualStyleBackColor = true;
            // 
            // DeleteFilm
            // 
            this.DeleteFilm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.DeleteFilm.Location = new System.Drawing.Point(196, 421);
            this.DeleteFilm.Name = "DeleteFilm";
            this.DeleteFilm.Size = new System.Drawing.Size(165, 28);
            this.DeleteFilm.TabIndex = 2;
            this.DeleteFilm.Text = "Удалить кино";
            this.DeleteFilm.UseVisualStyleBackColor = true;
            this.DeleteFilm.Click += new System.EventHandler(this.DeleteFilm_Click);
            // 
            // SaveAll
            // 
            this.SaveAll.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.SaveAll.Location = new System.Drawing.Point(9, 421);
            this.SaveAll.Name = "SaveAll";
            this.SaveAll.Size = new System.Drawing.Size(165, 28);
            this.SaveAll.TabIndex = 1;
            this.SaveAll.Text = "Сохранить всё";
            this.SaveAll.UseVisualStyleBackColor = true;
            this.SaveAll.Click += new System.EventHandler(this.SaveAll_Click);
            // 
            // FilmsTable
            // 
            this.FilmsTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.FilmsTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.FilmsTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdColumn,
            this.FilmNameColumn,
            this.BuysColumn,
            this.BuysCountColumn,
            this.BeginTimeColumn});
            this.FilmsTable.Location = new System.Drawing.Point(9, 24);
            this.FilmsTable.Name = "FilmsTable";
            this.FilmsTable.Size = new System.Drawing.Size(560, 391);
            this.FilmsTable.TabIndex = 0;
            // 
            // IdColumn
            // 
            this.IdColumn.DataPropertyName = "Id";
            this.IdColumn.HeaderText = "Id";
            this.IdColumn.Name = "IdColumn";
            this.IdColumn.Visible = false;
            // 
            // FilmNameColumn
            // 
            this.FilmNameColumn.DataPropertyName = "FilmName";
            this.FilmNameColumn.HeaderText = "Название";
            this.FilmNameColumn.Name = "FilmNameColumn";
            // 
            // BuysColumn
            // 
            this.BuysColumn.DataPropertyName = "Buys";
            this.BuysColumn.HeaderText = "Buys";
            this.BuysColumn.Name = "BuysColumn";
            this.BuysColumn.Visible = false;
            // 
            // BuysCountColumn
            // 
            this.BuysCountColumn.DataPropertyName = "BuysCount";
            this.BuysCountColumn.HeaderText = "Куплено";
            this.BuysCountColumn.Name = "BuysCountColumn";
            this.BuysCountColumn.ReadOnly = true;
            // 
            // BeginTimeColumn
            // 
            this.BeginTimeColumn.DataPropertyName = "BeginTime";
            this.BeginTimeColumn.HeaderText = "Время начала";
            this.BeginTimeColumn.Name = "BeginTimeColumn";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(585, 484);
            this.Controls.Add(this.Tabs);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Tabs.ResumeLayout(false);
            this.TicketsTab.ResumeLayout(false);
            this.TicketsTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TicketsCount)).EndInit();
            this.FilmsTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FilmsTable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl Tabs;
        private System.Windows.Forms.TabPage TicketsTab;
        private System.Windows.Forms.Button BuyButton;
        private System.Windows.Forms.NumericUpDown TicketsCount;
        private System.Windows.Forms.Label TicketsCountLabel;
        private System.Windows.Forms.ComboBox FilmComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage FilmsTab;
        private System.Windows.Forms.Button DeleteFilm;
        private System.Windows.Forms.Button SaveAll;
        private System.Windows.Forms.DataGridView FilmsTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn FilmNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuysColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuysCountColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn BeginTimeColumn;
    }
}

