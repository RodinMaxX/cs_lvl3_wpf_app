using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace Task3
{
    public class Film
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string FilmName { get; set; }

        public DateTime BeginTime { get; set; }

        public int BuysCount
        {
            get { return Buys.Sum(x => x.Count); }
        }

        public virtual ICollection<Buy> Buys { get; protected set; }

        public override string ToString()
        {
            return  this.FilmName;
        }
    }
}