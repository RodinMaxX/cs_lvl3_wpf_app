﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Task3;

namespace Lesson7HomeworkTask3
{
    public partial class Form1 : Form
    {
        private CinemaEngine cinema;
        
        public Form1()
        {
            InitializeComponent();
            cinema = new CinemaEngine();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            FilmsTable.DataSource = cinema.GetBinding();
            ReloadFilmComboBox();
        }

        private void ReloadFilmComboBox()
        {
            FilmComboBox.Items.Clear();
            foreach (Film film in (IEnumerable)FilmsTable.DataSource)
            {
                FilmComboBox.Items.Add(film);
            }
        }

        private void SaveAll_Click(object sender, EventArgs e)
        {
            cinema.Save();
            ReloadFilmComboBox();
        }

        private void DeleteFilm_Click(object sender, EventArgs e)
        {
            if (FilmsTable.CurrentRow != null)
            {
                cinema.DeleteRow(FilmsTable.CurrentRow.Cells["IdColumn"].Value);
            }
        }

        private void BuyButton_Click(object sender, EventArgs e)
        {
            var buy = new Buy((Film)FilmComboBox.SelectedItem, (int)TicketsCount.Value, DateTime.UtcNow);
            cinema.AddBuy(buy);
            cinema.Save();
        }


    }
}
